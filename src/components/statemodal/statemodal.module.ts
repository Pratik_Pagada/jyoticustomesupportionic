import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { StatemodalComponent } from './statemodal';

@NgModule({
  declarations: [
    StatemodalComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    StatemodalComponent
  ]
})
export class StatemodalComponentModule {}
