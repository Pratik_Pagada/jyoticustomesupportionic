import { Component } from '@angular/core';
import { NavParams, ViewController, LoadingController } from 'ionic-angular';

/**
 * Generated class for the ModalComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'modal',
  templateUrl: 'modal.html'
})
export class ModalComponent {

  text: string;
  data: any;
  companylists: any;
  auto_complete: any;
  loading: any;
  type:any;
  constructor(public navParams: NavParams, private viewCtrl: ViewController, public loadingCtrl: LoadingController) {
    this.showLoading();
    this.data = this.navParams.get('data');
    if(this.data.type){
        this.type = this.data.type;
    }
    this.companylists = [];
    this.auto_complete = [];
    this.companylists = this.data;

    this.auto_complete = this.data;

    this.hideLoading();
  }






  getItems(ev) {

    let val = ev.target.value;
    console.log(val);

    if (val && val.trim() != '') {
      this.companylists = this.auto_complete.filter((item) => {
          if(item.company){
        return (item.company.toLowerCase().indexOf(val.toLowerCase()) > -1);
          }
          else{
             return false
          }
      })
    }

  }
  clearItems(ev) {

    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.companylists = this.auto_complete.filter((item) => {
        return (item.company.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  selectedItem(item) {
    this.viewCtrl.dismiss(item);
  }

  back() {
    this.viewCtrl.dismiss();
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }



}
