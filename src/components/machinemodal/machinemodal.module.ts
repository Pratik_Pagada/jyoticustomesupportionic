import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MachinemodalComponent } from './machinemodal';

@NgModule({
  declarations: [
    MachinemodalComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    MachinemodalComponent
  ]
})
export class MachinemodalComponentModule {}
