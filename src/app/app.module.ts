import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { Camera } from '@ionic-native/camera';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestProvider } from '../providers/rest/rest';
import { StorageProvider } from '../providers/storage/storage';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { CamplaintregPage } from '../pages/camplaintreg/camplaintreg';
import { ModalComponent } from '../components/modal/modal';
import { StatemodalComponent } from '../components/statemodal/statemodal';
import { MachinemodalComponent } from '../components/machinemodal/machinemodal';
import { ComplaintsPage } from '../pages/complaints/complaints';
import { ComplaindetailsPage } from '../pages/complaindetails/complaindetails';
import { ComplainpicxPage } from '../pages/complainpicx/complainpicx';
import { ImageResizer } from '@ionic-native/image-resizer';
import { ProductmanualsPage } from '../pages/productmanuals/productmanuals';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { FileOpener } from '@ionic-native/file-opener';
import { BranchofficesPage } from '../pages/branchoffices/branchoffices';
import { Network } from '@ionic-native/network';
import { DvrPage } from '../pages/dvr/dvr';
import { AddDvrPage } from '../pages/add-dvr/add-dvr';
import { Geolocation } from '@ionic-native/geolocation';
import { InfopopupPage } from '../pages/infopopup/infopopup';
import { UpdateLocationPage } from '../pages/update-location/update-location';
import { IonicSelectableModule } from 'ionic-selectable';
import { Diagnostic } from '@ionic-native/diagnostic';
// import { Diagnostic } from '@ionic-native/diagnostic';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    ProfilePage,
    ChangepasswordPage,
    CamplaintregPage,
    ModalComponent,
    StatemodalComponent,
    MachinemodalComponent,
    ComplaintsPage,
    ComplaindetailsPage,
    BranchofficesPage,
    ComplainpicxPage,
    ProductmanualsPage,
    DvrPage,
    AddDvrPage,
    InfopopupPage,
    UpdateLocationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    IonicSelectableModule


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    ProfilePage,
    ChangepasswordPage,
    CamplaintregPage,
    ModalComponent,
    StatemodalComponent,
    MachinemodalComponent,
    ComplaintsPage,
    ComplaindetailsPage,
    ComplainpicxPage,
    ProductmanualsPage,
    BranchofficesPage,
    DvrPage,
    AddDvrPage,
    InfopopupPage,
    UpdateLocationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Camera ,
    FileTransfer,
 // FileUploadOptions,
  FileTransferObject,
  File,
  FileOpener ,
  SQLite,
  Network,
  Geolocation,
  Diagnostic,
    LocalNotifications,
    ImageResizer,
    RestProvider,
    StorageProvider
  ]
})
export class AppModule { }
