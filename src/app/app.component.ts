import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


import { StorageProvider } from '../providers/storage/storage';

import { RestProvider } from '../providers/rest/rest';
import { HomePage } from '../pages/home/home';

import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { CamplaintregPage } from '../pages/camplaintreg/camplaintreg';
import { ComplaintsPage } from '../pages/complaints/complaints';
import { ComplainpicxPage } from '../pages/complainpicx/complainpicx';
import { ProductmanualsPage } from '../pages/productmanuals/productmanuals';
import { BranchofficesPage } from '../pages/branchoffices/branchoffices';
import { DvrPage } from '../pages/dvr/dvr';
import { AddDvrPage } from '../pages/add-dvr/add-dvr';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  errorMessage: any;
  loading: any;
  pages: Array<{ title: string, component: any }>;

  constructor(public rest: RestProvider, private sqlite: SQLite, public loadingCtrl: LoadingController, private alertCtrl: AlertController, private toastCtrl: ToastController, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private storageService: StorageProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      //{ title: 'Home', component: HomePage },
      { title: 'Self Asigned Complaint Registration', component: CamplaintregPage },
      { title: 'Complaints', component: ComplaintsPage },
      { title: 'Product Manuals', component: ProductmanualsPage },
      { title: 'Branch Office', component: BranchofficesPage },
      { title: 'Profile', component: ProfilePage },
      { title: 'Change Password', component: ChangepasswordPage },

      { title: 'Logout', component: '' }
    ];





  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.overlaysWebView(true);
      this.splashScreen.hide();

      this.sqlite.create({
        name: 'salesdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS compainoffline(complain_id INTEGER PRIMARY KEY AUTOINCREMENT,customer_id TEXT,location TEXT,complain_subject TEXT,problem_detail TEXT,model_id TEXT,contact_person TEXT,contact_number TEXT,userid Text)', [])
          .then(res => console.log('compainoffline Executed SQL'))
          .catch(e => console.log(e));


      });

      this.sqlite.create({
        name: 'salesdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS companylistoffline(companyid INTEGER PRIMARY KEY AUTOINCREMENT,response LONGTEXT, userid Text)', [])
          .then(res => console.log('companylistoffline Executed SQL'))
          .catch(e => console.log(e));

      });

      this.sqlite.create({
        name: 'salesdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS complainsubject(complainsubjectid INTEGER PRIMARY KEY AUTOINCREMENT,respstatus LONGTEXT,userid Text)', [])
          .then(res => console.log('complainsubject Executed SQL'))
          .catch(e => console.log(e));

      });

    });
  }

  openPage(page) {
    if (page.title == 'Logout') {
      let confirm = this.alertCtrl.create({
        title: '<img src="assets/img/iconmonstr-warning-8.png" class="headerlogo">Logout',
        message: 'Are you sure you want to logout!',
        cssClass: 'myalert',
        buttons: [
          {
            text: 'CANCEL',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'OK',
            handler: () => {
              this.showLoading();
              this.rest.logout().subscribe(res => {
                if (res.status == 200) {
                  this.storageService.setObject('tokan', '');
                  this.nav.setRoot(LoginPage);
                  //this.presentAlert("Are you sure you want to logout!");
                }
                this.hideLoading();
              },
                error => {
                  this.errorMessage = <any>error;
                  this.hideLoading();
                  this.presentAlert(this.errorMessage.message.error);
                });
            }
          }
        ]
      });
      confirm.present();



    } else {
      this.nav.push(page.component);
    }
  }
  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      message: msg,
      title: '<img src="assets/img/iconmonstr-warning-8.png" class="headerlogo">Logout',
      buttons: ['ok'],
      cssClass: 'myalert'
    });
    alert.present();
  }


  async showLoading() {
    this.loading = await this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  async  hideLoading() {
    await this.loading.dismiss();
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }



}
