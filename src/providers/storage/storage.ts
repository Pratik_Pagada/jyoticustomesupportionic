import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class StorageProvider {

  constructor(public http: Http, public storage: Storage) {
    console.log('Hello StorageProvider Provider');
  }

  async getObject(key: any): Promise<any> {
    // return null
    return await this.storage.get(key).then((val) => {

      return val ? JSON.parse(val) : null;
    })
  }

  async setObject(key, obj) {
    var str = JSON.stringify(obj);
    await this.storage.set(key, str);

  }


}
