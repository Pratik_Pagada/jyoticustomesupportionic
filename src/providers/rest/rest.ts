import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { StorageProvider } from '../storage/storage';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  public baseurl: string = 'https://finix.jyoti.co.in/staging/service/mapi/'

  public tokan: string;
  constructor(public http: Http, private storageService: StorageProvider, public storage: Storage) {
    this.storageService.getObject('tokan').then((val) => {
      this.tokan = val;
    });
  }
  getTokan() {
    this.storageService.getObject('tokan').then((val) => {
      this.tokan = val;
      console.log(this.tokan);

    });
  }
  login(loginObj): Observable<any> {
    let headers = new Headers();

    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/engineer/login';
    let data = new FormData();

    data.append("employee_code", loginObj.username);
    data.append("password", loginObj.password);
    data.append("eDeviceType", 'Android');
    data.append("vPushToken", '123456');

    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }
  logout(): Observable<any> {
    let headers = new Headers();

    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });

    return this.http.get(this.baseurl + 'ws/v1/engineer/logout', reqOptions).map(this.extractData).catch(this.handleError);

  }

  companiesbyengineer(): Observable<any> {
    let headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/general/companiesbyengineer';
    let data = new FormData();

    return this.http.post(link,data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  companiesbyengineeroffline(): Observable<any> {
    let headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/general/companylistoffline';
    let data = new FormData();

    return this.http.post(link,data, reqOptions).map(this.extractData).catch(this.handleError);
  }


  productManual(): Observable<string[]> {
    return this.http.get('http://jyoti.co.in/webservice/products_manuals.php').map(this.extractData).catch(this.handleError);
  }
  DomesticList(): Observable<string[]> {
    let headers = new Headers();

    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link =  'http://jyoti.co.in/webservice/domestic-sales-service.php';



    return this.http.post(link, reqOptions).map(this.extractData).catch(this.handleError);

  }

  Internation(): Observable<string[]> {

    let headers = new Headers();

    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link =  'http://jyoti.co.in/webservice/internation-sales-service.php';



    return this.http.post(link, reqOptions).map(this.extractData).catch(this.handleError);
  }
  customerDetail(cid): Observable<any> {
    let headers = new Headers();

    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/general/companybyid';

    let data = new FormData();
    data.append("iCustId", cid);

    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  Selfcomplain(sccobj, custid, drp, modalid): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/complain/selfcreatecomplain';

    let data = new FormData();
    data.append("customer_id", custid);

    data.append("location", sccobj.address);

    data.append("complain_subject", drp);

    data.append("problem_detail", sccobj.dtl);

    data.append("model_id", modalid);

    data.append("contact_person", sccobj.contact_person);

    data.append("contact_number", sccobj.tel);

    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  Selfcomplainoffline(sccobj): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/complain/selfcreatecomplain';

    let data = new FormData();
    data.append("customer_id", sccobj.customer_id);

    data.append("location", sccobj.location);

    data.append("complain_subject", sccobj.complain_subject);

    data.append("problem_detail", sccobj.problem_detail);

    data.append("model_id",sccobj.model_id);

    data.append("contact_person", sccobj.contact_person);

    data.append("contact_number", sccobj.contact_number);

    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  proccedforcomplain(prdobj,mydata): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/complain/proceedcomplain';

    let data = new FormData();
    data.append("complain_id", mydata.id);

    data.append("machine_status", mydata.machine_status);

    data.append("serial_number",prdobj.image);

    data.append("machine_single_photo", prdobj.image1);

    data.append("machine_photo[]",prdobj.image2);

    //data.append("dVisitStartDate",prdobj.ddate);

    data.append("status", prdobj.status);

    data.append("complaintOpenOption", prdobj.complaintOpenOption);

    data.append("eMaterialReq", prdobj.eMaterialReq);

    data.append("make_remark",prdobj.textbox);


    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }



  alldropdownlistgen(): Observable<any> {
    let headers = new Headers();

    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/generalguest/alldropdownlistgen';

    let data = new FormData();
    data.append("dropdownId", '43');

    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  profile(profileobj): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/engineer/edit';

    let data = new FormData();
    data.append("first_name", profileobj.firstname);
    data.append("last_name", profileobj.lastname);
    data.append("phone", profileobj.phone);

    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  changepwd(pwdobj): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/engineer/changepassword';

    let data = new FormData();
    data.append("vOldPassword", pwdobj.oldPassword);
    data.append("vNewPassword", pwdobj.newPassword);


    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  ComplainList(cmpobj): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/complain/complainlist';

    let data = new FormData();
    data.append("vStatus", cmpobj.id);
    data.append("vKeyWord", cmpobj.KeyWord);

    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  RemoveMachinePhoto(rmpobj): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/complain/removemachinephoto';

    let data = new FormData();
    data.append("id", rmpobj);


    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }

  endvisit(envobj,env): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/complain/endvisit';

    let data = new FormData();
    data.append("complain_id", env.id);

    data.append("machine_single_photo", envobj);


    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }


  Getcomplaindetails(gcp): Observable<any> {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/complain/complaindetailbyid';

    let data = new FormData();
    data.append("complain_id", gcp.id);



    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }




  private extractData(res: Response) {
    let body = res.json();

    return body || {};
  }

  private extractPostData(res: Response) {
    let body = res;
    console.log(body);
    body = body.json();
    console.log(body);

    return body || {};
  }

  private handleError(error: Response | any) {

    let errMsg: string;
    let errObj: any;
    if (error instanceof Response) {
      const body = error.json() || '';
      //const err = body.error || JSON.stringify(body);
      //errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errObj = body;
      //errMsg = body.message.error;

    } else {
      errMsg = error.message ? error.message : error.toString();
    }


    if (errObj) {
      return Observable.throw(errObj);
    } else {
      errObj = {};
      errObj.message.error = "Server error";
      Observable.throw(errObj);
    }

  }

  handleError1(error: any) {

    let errorMsg = error.message || 'Network Error ! Try again later.';
    return Observable.throw(errorMsg);
  }


  getDVR(){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/dvrlist';

    return this.http.post(link, [], reqOptions).map(this.extractData).catch(this.handleError);
  }
  getDVRDetails(dvrId){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/dvrdetails';
    let data = new FormData();
    data.append("iDvrId", dvrId);
    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }
  startDVR(lat,long){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/startdvr';
    let data = new FormData();
    data.append("vStartLat", lat);
    data.append("vStartLong", long);

    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }
  startCoDVR(dvrId,lat,long,coEndId)
  {
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/StartCoDvr';
    let data = new FormData();
    data.append("vStartLat", lat);
    data.append("vStartLong", long);
    data.append("iCoEngId", coEndId);
    data.append("iDvrId", dvrId);
    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }
  endCoDvr(dvrId,lat,long,coEndId){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    headers.append('content-type', 'application/json');
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/EndCoDvr';
    var obj={
        vEndLat:lat,
        vEndLong:long,
        iCoDvrId:coEndId,
        iDvrId:dvrId
    }
    return this.http.post(link, obj, reqOptions).map(this.extractData).catch(this.handleError);
  }
  endDvr(obj){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    headers.append('content-type', 'application/json');
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/enddvr';
    return this.http.post(link, obj, reqOptions).map(this.extractData).catch(this.handleError);
  }

  updateLocation(obj){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/updatelocation';
    let data = new FormData();
    data.append("iLocationId", obj.iLocationId);
    data.append("iVisitType", obj.iVisitType);
    data.append("iCustomerId", obj.iCustomerId);
    data.append("vPurpose", obj.vPurpose);
    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }
  getCoTravllerList(dvrId){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/CoTravellerList';
    let data = new FormData();
    data.append("iDvrId", dvrId);
    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }
  getCoEngineerList(){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/CoEngList';
    return this.http.post(link, [], reqOptions).map(this.extractData).catch(this.handleError);
  }
  getFuelType(){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/fueltypelist';
    return this.http.post(link, [], reqOptions).map(this.extractData).catch(this.handleError);

  }
  getVisitTypeList(){

    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/visitTypelist';
    return this.http.post(link, [], reqOptions).map(this.extractData).catch(this.handleError);
  }
  getCustomerList(){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/complain/complainlistdropdown';
    return this.http.post(link, [], reqOptions).map(this.extractData).catch(this.handleError);
  }


  getVehicleType(){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/vehicletypelist';
    return this.http.post(link, [], reqOptions).map(this.extractData).catch(this.handleError);
  }
  getLocationList(dvrId){
    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/locationlist';
    let data = new FormData();
    data.append("iDvrId", dvrId);
    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);

  }
  pinLocation(dvrId,lat,long,locationNo){

    let headers: Headers = new Headers();
    headers.append('Vauthtoken', 'Bearer ' + this.tokan);
    let reqOptions = new RequestOptions({ headers: headers });
    let link = this.baseurl + 'ws/v1/dvr/pinlocation';
    let data = new FormData();
    data.append("iDvrId", dvrId);
    data.append("vLatitude", lat);
    data.append("vLongitude", long);
    data.append("iLocationNo", locationNo);
    return this.http.post(link, data, reqOptions).map(this.extractData).catch(this.handleError);
  }
}
