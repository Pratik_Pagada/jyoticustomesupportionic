import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController, ToastController, Content } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import { DomSanitizer } from '@angular/platform-browser';
import { StorageProvider } from '../../providers/storage/storage';
import { RestProvider } from '../../providers/rest/rest';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoginPage } from '../login/login';
import * as $ from 'jquery';
/**
 * Generated class for the ComplainpicxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-complainpicx',
  templateUrl: 'complainpicx.html',
})
export class ComplainpicxPage {
  cmpid: any;
  private image: string;
  private image1: string;
  private image2: string;
  private pix: string;
  private pixslct: any;
  loading: any;
  @ViewChild(Content) content: Content;

  private Myform: FormGroup;
  errorMessage: any;
  pixslctid: any;
  currentDate: any;
  enddate: any;
  private image3: string;
  Datalist: any;
  constructor(private camera: Camera, private imageResizer: ImageResizer, private storageService: StorageProvider, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private formBuilder: FormBuilder, private toastCtrl: ToastController
    , public alertCtrl: AlertController,
    private domSanitizer: DomSanitizer) {
    this.cmpid = this.navParams.get('comlist');
    this.Searchbtn();
    this.Datalist = {};
    this.image = this.cmpid.serial_number;
    this.Myform = this.formBuilder.group({
      image: [this.image, Validators.required],
      image1: [this.image1, Validators.required],
      textbox: [''],
      image2: [this.image2],

      status: [''],
      eMaterialReq: [''],
      complaintOpenOption: [''],
      date1: [''],
      date2: ['']
    });
    this.cmpid.eMaterialReq = 'no';
    if (this.cmpid.status = '1') {
      this.cgt = false;
    }
    else if (this.cmpid.status = '3') {
      this.cgt = true;
    }
  }
  removeimage(item) {
    let confirm = this.alertCtrl.create({
      title: 'Confrim Remove Image',
      message: 'Are you sure to remove?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.rest.RemoveMachinePhoto(item.id).subscribe(data => {

              if (data.status == 200) {
                this.showToast(data.message.success);
                console.log(item.id);
                let index = this.pixslct.indexOf(item);
                if(index > -1){
                this.pixslct.splice(index, 1);
              }
              }
            },
              error => {
                this.errorMessage = <any>error;
                this.hideLoading();
                this.showToast(this.errorMessage.message.error);
              });
          }
        }
      ]
    });
    confirm.present();

  }
  fileChange(event) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();

      reader.onload = (event: any) => {
        this.image = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
    let fileList: FileList = event.target.files;
    let file: File = fileList[0];
    console.log(file);
  }

  onChange(val: any) {
    console.log(val);
    if (val == '1') {
      this.cgt = false;
    }
    else if (val == '3') {
      this.cgt = true;
    }
  }
  emrChange(val){
    if (val == 'no') {
    this.Datalist.eMaterialReq = val;
    }
    else if (val == 'yes') {
     this.Datalist.eMaterialReq =val;
    }
  }

  cgt: boolean = true;
  cn: any;
  button: boolean = true;
  btndtl: boolean = true;
  radio_select(value) {
    if (value == 'cgt') {
      this.cgt = false;
      this.cn = 1;

    } else if (value == 'noncgt') {
      this.cgt = true;
      this.cn = 3;
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ComplainpicxPage');
  }

  onTakePicture() {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      saveToPhotoAlbum: true,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      this.image = 'data:image/jpeg;base64,' + imageData;
      this.Myform.controls['image'].setValue(this.image ? this.image : '');
    }, (err) => {
      // this.displayErrorAlert(err);
    });
  }

  onTakePictures1() {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      saveToPhotoAlbum: true,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((image1Data) => {
      this.image1 = 'data:image1/jpeg;base64,' + image1Data;
      this.Myform.controls['image1'].setValue(this.image1 ? this.image1 : '');
    }, (err) => {
      //this.displayErrorAlert(err);
    });
  }

  onTakePictures2() {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      saveToPhotoAlbum: true,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((image2Data) => {
      this.image2 = 'data:image2/jpeg;base64,' + image2Data;
      this.Myform.controls['image2'].setValue(this.image2 ? this.image2 : '');
    }, (err) => {
      //this.displayErrorAlert(err);
    });
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  MyformSubmit() {
    if (this.Myform.valid) {
      this.showLoading();

      this.rest.proccedforcomplain(this.Myform.value, this.Datalist).subscribe(data => {
        this.hideLoading();
        if (data.status == 200) {
          console.log(this.Datalist);
          this.showToast(data.message.success);
          this.Searchbtn();
          this.button = false;
          this.btndtl = false;
        }
      },
        error => {
          this.errorMessage = <any>error;
          this.hideLoading();
          if (this.errorMessage.status == 401) {

            this.storageService.setObject("tokan", '');
            this.navCtrl.setRoot(LoginPage);
          }
          this.showToast(this.errorMessage.message.error);
        });
    } else {
      this.validateAllFormFields(this.Myform); //{7}
      this.content.scrollToTop();
    }

  }

  onTakePictures3() {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      saveToPhotoAlbum: true,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((image3Data) => {



      this.showLoading();

      this.rest.endvisit(image3Data, this.Datalist).subscribe(data => {
        this.hideLoading();
        if (data.status == 200) {
          this.button = true;
          this.showToast(data.message.success);
          this.Searchbtn();
        
        }
      },
        error => {
          this.errorMessage = <any>error;
          this.hideLoading();
          if (this.errorMessage.status == 401) {

            this.storageService.setObject("tokan", '');
            this.navCtrl.setRoot(LoginPage);
          }
          this.showToast(this.errorMessage.message.error);
        });

    }, (err) => {
      //this.displayErrorAlert(err);
    });
  }

  Searchbtn() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.rest.Getcomplaindetails(this.cmpid).subscribe(
      res => {
        if (res.status == 200) {
          this.Datalist = res.data[0];
          console.log(this.Datalist);
          this.image = this.Datalist.serial_number;
          var d = 0;
          for (let i = 0; i < this.Datalist.machine_photos.length; i++) {
            if (this.Datalist.machine_photos[i]) {
              d++;
              this.pixslct = this.Datalist.machine_photos;

              this.image1 = this.Datalist.machine_photos[i].machine_photo_url;
              //pix.machine_photo_url  
            }

          }

          for (let i = 0; i < this.Datalist.complain_visits.length; i++) {
            if (this.Datalist.complain_visits[i]) {
              this.currentDate = this.Datalist.complain_visits;
              // this.enddate = this.Datalist.complain_visits[i].dVisitEndDate;  
            }

          }
          this.Datalist.complaintOpenOption;
          this.Datalist.status;
          this.Datalist.eMaterialReq;
          if (this.Datalist.visitstatus == '1') {
            this.button = false;
            this.btndtl = false;

           
            $('#button').width('50%');
          }
          else if (this.Datalist.visitstatus == '0') {
            this.button = true;
            this.btndtl = false;
            $('#button').width('100%');
          }

        }
        loading.dismiss();
      },
      error => {
        this.errorMessage = <any>error;
        loading.dismiss();
        if (this.errorMessage.status == 401) {
          //alert(this.errorMessage.message.error);
          this.storageService.setObject("tokan", '');
          this.navCtrl.setRoot(LoginPage);
        }

      });
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  back() {
    this.navCtrl.pop();
  }
  displayErrorAlert(err) {
    console.log(err);
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Error while trying to capture picture',
      buttons: ['OK']
    });
    alert.present();
  }
}
