import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComplainpicxPage } from './complainpicx';

@NgModule({
  declarations: [
    ComplainpicxPage,
  ],
  imports: [
    IonicPageModule.forChild(ComplainpicxPage),
  ],
})
export class ComplainpicxPageModule {}
