import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Platform } from 'ionic-angular';
import { AddDvrPage } from '../add-dvr/add-dvr';
import { RestProvider } from '../../providers/rest/rest';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

/**
 * Generated class for the DvrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-dvr',
    templateUrl: 'dvr.html',
})
export class DvrPage {
    arrDvr = [];
    errorMessage: any;
    lat = "";
    long = "";
    constructor(private diagnostic: Diagnostic,private storageService: StorageProvider, public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private toastCtrl: ToastController, private geolocation: Geolocation, public platform: Platform) {

    }
    ionViewWillEnter() {
        var gpsOptions={
           enableHighAccuracy:true
        }
        this.geolocation.getCurrentPosition(gpsOptions).then((resp) => {
            console.log(resp.coords.latitude);
            console.log(resp.coords.longitude);
            this.lat = resp.coords.latitude+"";
            this.long = resp.coords.longitude+"";
        }).catch((error) => {
            console.log('Error getting location', error);
        });

        let watch = this.geolocation.watchPosition(gpsOptions);
        watch.subscribe((resp) => {
            console.log(resp.coords.latitude);
            console.log(resp.coords.longitude);
            this.lat = resp.coords.latitude+"";
            this.long = resp.coords.longitude+"";
        });

        this.getDvrList('');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DvrPage');
        // this.getDvrList();

        this.arrDvr = [];
    }

    doRefresh(refresh){
        this.getDvrList(refresh);
    }
    getDvrList(refresh) {
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });

        loading.present();

        this.rest.getDVR().subscribe(
            res => {
                loading.dismiss();
                if (res.status == 200) {
                    if(refresh){
                        refresh.complete();
                    }
                    this.arrDvr = res.data;

                    console.log(this.arrDvr);

                }
            },
            error => {
            this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    //alert(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }

            });
    }

    back() {
        this.navCtrl.pop();
    }
    onClickAdd() {

        this.diagnostic.isGpsLocationEnabled().then((isAvailable)=>{
            console.log(isAvailable);
            if(isAvailable){
                let loading = this.loadingCtrl.create({
                    cssClass: 'transparent'
                });
                loading.present();
                var gpsOptions={
                   enableHighAccuracy:true
                }
                this.geolocation.getCurrentPosition(gpsOptions).then((resp) => {
                    console.log(resp.coords.latitude);
                    console.log(resp.coords.longitude);
                    this.lat = resp.coords.latitude+"";
                    this.long = resp.coords.longitude+"";
                    this.rest.startDVR(this.lat, this.long).subscribe(
                        res => {
                            console.log(res);

                            loading.dismiss();
                            if (res.status == 200) {

                                this.getDvrDetails(res.data.iDvrId);
                            }
                        },
                        error => {
                            this.errorMessage = <any>error;
                            console.log(this.errorMessage);

                            loading.dismiss();
                            if (this.errorMessage.status == 401) {
                                //alert(this.errorMessage.message.error);
                                this.storageService.setObject("tokan", '');
                                this.navCtrl.setRoot(LoginPage);
                            } else {
                                this.showToast(this.errorMessage.message.error);
                            }

                        });

        }).catch((error) => {
                console.log('Error getting location', error);
                loading.dismiss();
                this.showToast("Please enable location service");
            });
        }
        else{
            this.showToast("Please enable location service");
            this.diagnostic.switchToLocationSettings();
        }
    })
        .catch((err)=>{

        });









    }
    getDvrDetails(dvrId) {
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        this.rest.getDVRDetails(dvrId).subscribe(
            res => {
                loading.dismiss();
                if (res.status == 200) {
                    this.navCtrl.push(AddDvrPage, { dvrDetails: res.data[0],lat:this.lat,long:this.long })

                }

            },
            error => {
            this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {

                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }
                else {
                    this.showToast(this.errorMessage.message.error);
                }

            });

    }
    showToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }
}
