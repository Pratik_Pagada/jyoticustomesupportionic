import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RestProvider } from '../../providers/rest/rest';
import { LoginPage } from '../login/login';
import { ComplaindetailsPage } from '../complaindetails/complaindetails';

/**
 * Generated class for the ComplaintsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-complaints',
  templateUrl: 'complaints.html',
})
export class ComplaintsPage {
  loading: any;
  errorMessage: any;
  comlist:any;
  selectedValue={id:'1',KeyWord:''};

  constructor(private storageService: StorageProvider, public modalCtrl: ModalController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
    this.Searchbtn();
    
  }
  
  Searchbtn() {
	  let loading = this.loadingCtrl.create({
      cssClass: 'transparent'
       });

     loading.present();
	 
    this.rest.ComplainList(this.selectedValue).subscribe(
	   res => {
	    if(res.status ==200){ 
        this.comlist =  res.data;
        loading.dismiss();
		   }
	   loading.dismiss();
	   },
	 error =>  {this.errorMessage = <any>error;
	  loading.dismiss();
	   if(this.errorMessage.status == 401){ 
	          //alert(this.errorMessage.message.error);
		      this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	   }
	  
	 });
  }

  cmpdetail(cid) {
    this.navCtrl.push(ComplaindetailsPage,{'lead':cid}); 
    
   }

   
  cn=[1];
  radio_select(value) {
    if (value == 'cgt') {
     if(this.selectedValue.id = '1'){
       this.Searchbtn();
     }
      
      
    } else if (value == 'noncgt') {
     
      if(this.selectedValue.id = '2'){
        this.Searchbtn();
      }
    
    }
  }

  doRefresh(refresh){

    this.rest.ComplainList(this.selectedValue).subscribe(res => {
      this.comlist=res.data;
     console.log(this.comlist) /// data is not update, still the same
    })
 
   setTimeout(() => {
      this.rest.ComplainList(this.selectedValue).subscribe(res => {  /// I am trying to call the service within setTimeout , but still data is not updating
        this.comlist=res.data;
        refresh.complete(); 
          console.log(this.comlist)
  })  
}, 1000);
}


  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  back() {
    this.navCtrl.popToRoot();
  }
}
