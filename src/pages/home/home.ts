import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { CamplaintregPage } from '../camplaintreg/camplaintreg';

import { ComplaintsPage } from '../complaints/complaints';
import { ProductmanualsPage } from '../productmanuals/productmanuals';
import { BranchofficesPage } from '../branchoffices/branchoffices';
import { ProfilePage } from '../profile/profile';
import { Network } from '@ionic-native/network';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { StorageProvider } from '../../providers/storage/storage';
import { DvrPage } from '../dvr/dvr';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  errorMessage: any;
  loginid: any;
  items: any;
  companyid: any;
  drpid: any;
  constructor(private network: Network, private sqlite: SQLite, private storageService: StorageProvider, public navCtrl: NavController, public rest: RestProvider) {
    this.rest.getTokan();
    this.storageService.getObject('id').then((val) => {
      if (val) {
        this.loginid = val;
      }
    });
    this.storageService.getObject('tokan').then((val) => {
      if (val) {
    this.rest.companiesbyengineeroffline().subscribe(data => {
      if (data.status == 200) {
        //console.log(data.data);
        const myObjStr = JSON.stringify(data.data);

        //console.log(myObjStr);
        // "{"name":"Skip","age":2,"favoriteFood":"Steak"}"

        // console.log(JSON.parse(myObjStr));
        // Object {name:"Skip",age:2,favoriteFood:"Steak"}

        this.sqlite.create({
          name: 'salesdb.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          console.log(this.loginid);
          db.executeSql("SELECT * FROM companylistoffline WHERE userid=? LIMIT 1", [this.loginid]).then((data) => {
            this.items = [];
            console.log(data.rows.length);
            if (data.rows.length > 0) {
              console.log("UPDATE");
              for (var i = 0; i < data.rows.length; i++) {
                this.items.push(data.rows.item(i));
                //  console.log(this.items[0].response);
                this.companyid = JSON.parse(this.items[i].companyid);

                console.log(this.companyid)
                this.sqlite.create({
                  name: 'salesdb.db',
                  location: 'default'
                }).then((db: SQLiteObject) => {
                  db.executeSql('UPDATE companylistoffline SET response = ? WHERE companyid = ?', [myObjStr, this.companyid])
                    .then(res => {
                      console.log(res);

                    })
                    .catch(e => {
                      console.log(e);

                    });
                }).catch(e => {
                  console.log(e);

                });

              }


            } else {
              console.log("INSERT");
              this.sqlite.create({
                name: 'salesdb.db',
                location: 'default'
              }).then((db: SQLiteObject) => {
                db.executeSql('INSERT INTO companylistoffline VALUES(?,?,?)', [, myObjStr, this.loginid])
                  .then(res => {
                    console.log(res);

                  })
                  .catch(e => {
                    console.log(e);

                  });
              }).catch(e => {
                console.log(e);
              });
            }
          }, (e) => {

            console.log("Errot: " + JSON.stringify(e));
          });
        });




        /* var d = 0;
         for (let i = 0; i < data.data.length; i++) {
           //console.log(data.data[i].id);
           this.id = data.data[i].id;
           this.company = data.data[i].company;
           this.address = data.data[i].address;
     
           if (data.data[i].companymodels) {
             this.companymodallist = data.data[i].companymodels;
             for (let i = 0; i < this.companymodallist.length; i++) {
               //console.log(this.companymodallist[i].id);
               this.cmpid = this.companymodallist[i].id;
               this.customerid = this.companymodallist[i].customer_id;
               this.productname = this.companymodallist[i].product_name;
               this.machineserialnumber = this.companymodallist[i].machine_serial_number;
               this.dispatchdate = this.companymodallist[i].dispatch_date;
               this.uwowamc =this.companymodallist[i].uw_ow_amc;
               this.machinestatusid =  this.companymodallist[i].Machine_Status_Id;
             }
           }
             //console.log("magic",this.id,this.company,this.address,this.cmpid,this.customerid,this.productname,this.machineserialnumber,this.dispatchdate,this.uwowamc,this.machinestatusid);
         }*/

      }
    },
      error => {
        this.errorMessage = <any>error;
        //this.hideLoading();
        //this.showToast(this.errorMessage.message.error);
      });
    }
  });

  this.rest.alldropdownlistgen().subscribe(data => {
    if (data.status == 200) {
      
      const myObjStr = JSON.stringify(data.data);
     
    this.sqlite.create({
      name: 'salesdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      console.log("loginid dropdown",this.loginid);
      db.executeSql("SELECT * FROM complainsubject WHERE userid=? LIMIT 1",[this.loginid]).then((data) => {
        this.items = [];
        console.log(data.rows.length);
        if (data.rows.length > 0) {
          console.log("UPDATE");
          for (var i = 0; i < data.rows.length; i++) {
            this.items.push(data.rows.item(i));
          
            this.drpid =JSON.parse(this.items[i].complainsubjectid); 
           
            console.log("drpid",this.drpid)
            this.sqlite.create({
              name: 'salesdb.db',
              location: 'default'
              }).then((db: SQLiteObject) => {
              db.executeSql('UPDATE complainsubject SET respstatus = ? WHERE complainsubjectid = ?',[myObjStr,this.drpid])                  
                .then(res => {
                console.log(res);
                
                })
                .catch(e => {
                console.log(e);
                 
                });
              }).catch(e => {
              console.log(e);
               
              });

          }


        }else{
          console.log("INSERT");
          this.sqlite.create({
            name: 'salesdb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('INSERT INTO complainsubject VALUES(?,?,?)', [, myObjStr,this.loginid])
              .then(res => {
                console.log(res);
  
              })
              .catch(e => {
                console.log(e);
  
              });
          }).catch(e => {
            console.log(e);
          });
        }
      }, (e) => {

        console.log("Errot: " + JSON.stringify(e));
      });
    });
     
      /* this.sqlite.create({
        name: 'salesdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        db.executeSql('INSERT INTO complainsubject VALUES(?,?)', [, myObjStr])
          .then(res => {
            console.log(res);

          })
          .catch(e => {
            console.log(e);

          });
      }).catch(e => {
        console.log(e);

      });*/


    }
  }, error => {
    this.errorMessage = <any>error;
    //this.showToast(this.errorMessage.message.error);
  }
  )
  }
  selfcamplaint() {
    this.navCtrl.push(CamplaintregPage);
  }
  editprofile() {
    this.navCtrl.push(ProfilePage);
  }
  camplaint() {
    this.navCtrl.push(ComplaintsPage);
  }
  addCustomer() {
    this.navCtrl.push(ProductmanualsPage);
  }
  branch() {
    this.navCtrl.push(BranchofficesPage);
  }
  mangeDVR(){
    this.navCtrl.push(DvrPage)
  }
}
