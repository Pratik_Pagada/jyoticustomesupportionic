import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController, Platform, ModalController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RestProvider } from '../../providers/rest/rest';
import { LoginPage } from '../login/login';
import { Geolocation } from '@ionic-native/geolocation';
import { InfopopupPage } from '../infopopup/infopopup';
import { UpdateLocationPage } from '../update-location/update-location';
import { Diagnostic } from '@ionic-native/diagnostic';

/**
 * Generated class for the AddDvrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-add-dvr',
    templateUrl: 'add-dvr.html',
})
export class AddDvrPage {
    // dvrDetails = {
    //   "iDvrId": "63158",
    //   "dCreatedDate": "02:49 PM | 20-02-2019",
    //   "eRunningStatus": "End",
    //   "vStartAddress": "Unnamed Road, Chamundi Nagar, Peenya, Bengaluru, Karnataka 560058, Indie",
    //   "vEndAddress": "18B, Newland, College Square, Kolkata, West Bengal 700012, Indie",
    //   "iTransportType": "2 Wheeler",
    //   "iFuelType": "Petrol",
    //   "vFoodAllowance": "500.00",
    //   "vKms": "5632.72",
    //   "vAmount": "19243.44",
    //   "ePaymentStatus": "Pending"
    // };
    dvrDetails: any;
    errorMessage: any;
    lat = "";
    long = "";

    arrVisit = [];
    strCoTraveller = "";
    arrVehicleType = [];
    arrFuel = [];
    arrVisitType = [];
    arrCustomer = [];
    constructor(private diagnostic: Diagnostic, public modalCtrl: ModalController, private storageService: StorageProvider, public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private toastCtrl: ToastController, private geolocation: Geolocation, public platform: Platform, public alertCtrl: AlertController) {
        this.dvrDetails = navParams.get('dvrDetails');
        this.lat = navParams.get('lat');
        this.long = navParams.get('long');
        // this.platform.ready().then(() => {

        // })
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AddDvrPage');
    }
    ionViewWillEnter() {
        var gpsOptions = {
            enableHighAccuracy: true
        }
        this.geolocation.getCurrentPosition(gpsOptions).then((resp) => {
            console.log(resp.coords.latitude);
            console.log(resp.coords.longitude);
            this.lat = resp.coords.latitude + "";
            this.long = resp.coords.longitude + "";
        }).catch((error) => {
            console.log('Error getting location', error);
        });

        let watch = this.geolocation.watchPosition(gpsOptions);
        watch.subscribe((resp) => {
            console.log(resp.coords.latitude);
            console.log(resp.coords.longitude);
            this.lat = resp.coords.latitude + "";
            this.long = resp.coords.longitude + "";
        });
        this.getVisitDetails()
    }
    back() {
        this.navCtrl.pop();
    }
    onClickStartDVR() {

    }
    onClickAddTraveller() {
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        let alert = this.alertCtrl.create({
            cssClass: "alert-popup",
        });
        alert.setTitle('Add Co-Traveller');

        this.rest.getCoEngineerList().subscribe(
            res => {
                loading.dismiss();
                if (res.status == 200) {
                    for (let index = 0; index < res.data.length; index++) {
                        const element = res.data[index];
                        console.log(element);

                        alert.addInput(
                            {
                                "value": element.id,
                                "label": element.ename,
                                type: 'radio',
                            })
                    }
                    alert.present();

                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }
                else {
                    this.showToast(this.errorMessage.message.error);
                }

            });

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',

            handler: data => {
                let loading = this.loadingCtrl.create({
                    cssClass: 'transparent'
                });

                this.diagnostic.isGpsLocationEnabled().then((isAvailable) => {
                    console.log(isAvailable);
                    if (isAvailable) {
                        var gpsOptions = {
                            enableHighAccuracy: true
                        }
                        this.geolocation.getCurrentPosition(gpsOptions).then((resp) => {
                            this.lat = resp.coords.latitude + "";
                            this.long = resp.coords.longitude + "";
                            this.rest.startCoDVR(this.dvrDetails.iDvrId, this.lat, this.long, data).subscribe(
                                res => {
                                    loading.dismiss();
                                    if (res.status == 200) {
                                        this.showToast(res.message.success);
                                        this.getCoTravellers();
                                    }
                                },
                                error => {
                                    this.errorMessage = <any>error;
                                    loading.dismiss();
                                    if (this.errorMessage.status == 401) {
                                        this.showToast(this.errorMessage.message.error);
                                        this.storageService.setObject("tokan", '');
                                        this.navCtrl.setRoot(LoginPage);
                                    }

                                });
                        })
                    }
                    else {
                        loading.dismiss();
                        this.showToast("Please enable location service");
                        this.diagnostic.switchToLocationSettings();
                    }
                })
            }
        });


    }
    OnClickRemoveTraveller() {

        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        let alert = this.alertCtrl.create({
            cssClass: "alert-popup",
        });
        alert.setTitle('Add Co-Traveller');

        this.rest.getCoTravllerList(this.dvrDetails.iDvrId).subscribe(
            res => {
                loading.dismiss();
                if (res.status == 200) {
                    for (let index = 0; index < res.data.length; index++) {
                        const element = res.data[index];
                        alert.addInput(
                            {
                                "value": element.iCoDvrId,
                                "label": element.EngName,
                                type: 'radio',
                            })
                    }
                    alert.present();

                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }
                else {
                    this.showToast(this.errorMessage.message.error);
                }

            });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {
                let loading = this.loadingCtrl.create({
                    cssClass: 'transparent'
                });
                loading.present();
                this.diagnostic.isGpsLocationEnabled().then((isAvailable) => {
                    console.log(isAvailable);
                    if (isAvailable) {
                        let option = {
                            enableHighAccuracy: true
                        }
                        this.geolocation.getCurrentPosition(option).then((resp) => {
                            console.log(resp.coords.latitude);
                            console.log(resp.coords.longitude);
                            this.lat = resp.coords.latitude + "";
                            this.long = resp.coords.longitude + "";
                            this.rest.endCoDvr(this.dvrDetails.iDvrId, this.lat, this.long, data).subscribe(
                                res => {
                                    loading.dismiss();
                                    if (res.status == 200) {
                                        this.showToast(res.message.success);
                                        this.getCoTravellers();
                                    }
                                },
                                error => {
                                    this.errorMessage = <any>error;
                                    loading.dismiss();
                                    if (this.errorMessage.status == 401) {
                                        this.showToast(this.errorMessage.message.error);
                                        this.storageService.setObject("tokan", '');
                                        this.navCtrl.setRoot(LoginPage);
                                    }
                                    else {
                                        this.showToast(this.errorMessage.message.error);
                                    }

                                });

                        }).catch((error) => {
                            loading.dismiss();
                            console.log('Error getting location', error);
                            this.showToast("Please enable location service");
                        });
                    }
                    else {
                        loading.dismiss();
                        this.showToast("Please enable location service");
                        this.diagnostic.switchToLocationSettings();
                    }
                })
            }
        });


    }

    getVehicleType() {
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        this.rest.getVehicleType().subscribe(
            res => {
                loading.dismiss();
                if (res.status == 200) {
                    this.arrVehicleType = res.data;
                    this.getFuelType();
                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }
                else {
                    this.showToast(this.errorMessage.message.error);
                }

            });
    }
    getFuelType() {
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        this.rest.getFuelType().subscribe(
            res => {
                loading.dismiss();
                if (res.status == 200) {
                    this.arrFuel = res.data;
                    this.onClickEndDvr();
                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }
                else {
                    this.showToast(this.errorMessage.message.error);
                }

            });
    }
    onClickEndDvr() {
        // if(this.lat == ""){
        //     this.showToast("Please enable location service");
        //     return
        // }

        const modal = this.modalCtrl.create(InfopopupPage, { lat: this.lat, long: this.long, dvrId: this.dvrDetails.iDvrId, arrVehicle: this.arrVehicleType, arrFuel: this.arrFuel });
        modal.present();


        modal.onDidDismiss(data => {
            console.log(data);
            if (data) {
                this.navCtrl.pop();
            }
        });
    }
    onClickPinLocation() {
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        loading.present();
        let option={
            enableHighAccuracy:true
        }
        this.diagnostic.isGpsLocationEnabled().then((isAvailable) => {
            console.log(isAvailable);
            if (isAvailable) {
        this.geolocation.getCurrentPosition(option).then((resp) => {
            console.log(resp.coords.latitude);
            console.log(resp.coords.longitude);
            this.lat = resp.coords.latitude+"";
            this.long = resp.coords.longitude+"";
        this.rest.pinLocation(this.dvrDetails.iDvrId, this.lat, this.long, "1").subscribe(
            res => {
                loading.dismiss();
                if (res.status == 200) {
                    this.showToast(res.message.success);
                    this.getVisitDetails();
                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }

            });
        })
    }
    else {
        loading.dismiss();
        this.showToast("Please enable location service");
        this.diagnostic.switchToLocationSettings();
    }
})
    }

    getVisitDetails() {

        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        this.rest.getLocationList(this.dvrDetails.iDvrId).subscribe(
            res => {
                this.arrVisit = [];
                loading.dismiss();
                if (res.status == 200) {
                    // this.showToast(res.message.success);
                    this.arrVisit = res.data;
                    this.getCoTravellers();
                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }

            });
    }

    getCoTravellers() {
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        this.rest.getCoTravllerList(this.dvrDetails.iDvrId).subscribe(
            res => {
                loading.dismiss();
                if (res.status == 200) {
                    if (res.data.length > 0) {
                        var arrCoTravel = [];
                        for (let index = 0; index < res.data.length; index++) {
                            const element = res.data[index];
                            arrCoTravel.push(index + 1 + "." + element.EngName);
                        }
                        this.strCoTraveller = arrCoTravel.toString();

                    }
                    else {
                        this.strCoTraveller = ""
                    }

                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }
                else {
                    this.showToast(this.errorMessage.message.error);
                }

            });
    }

    showToast(msg) {

        let toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }
    gotoVisitDetails(visit) {
        console.log(visit);
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        this.rest.getVisitTypeList().subscribe(
            res => {
                this.arrVisitType = [];
                loading.dismiss();
                if (res.status == 200) {
                    // this.showToast(res.message.success);
                    this.arrVisitType = res.data;
                    this.getCustomerList(visit);
                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }
                else {
                    this.showToast(this.errorMessage.message.error);
                }

            });

    }
    getCustomerList(visit) {
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        this.rest.getCustomerList().subscribe(
            res => {
                this.arrCustomer = [];
                loading.dismiss();
                if (res.status == 200) {
                    this.arrCustomer = res.data;
                    console.log(this.arrCustomer);

                    const modal = this.modalCtrl.create(UpdateLocationPage, {
                        arrVisitType: this.arrVisitType, arrCustomer: this.arrCustomer,
                        visit: visit, selectedVisit: visit.iVisitType, Purpose: visit.vPurpose, selectedCustomerId: visit.iCustomerId
                    });
                    modal.present();
                    modal.onDidDismiss(data => {
                        if (data) {
                            this.getVisitDetails();
                        }
                    });
                }
            },
            error => {
                this.errorMessage = <any>error;
                loading.dismiss();
                if (this.errorMessage.status == 401) {
                    this.showToast(this.errorMessage.message.error);
                    this.storageService.setObject("tokan", '');
                    this.navCtrl.setRoot(LoginPage);
                }
                else {
                    this.showToast(this.errorMessage.message.error);
                }
            });

    }
}

