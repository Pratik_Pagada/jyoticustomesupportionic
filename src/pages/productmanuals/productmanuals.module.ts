import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductmanualsPage } from './productmanuals';

@NgModule({
  declarations: [
    ProductmanualsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductmanualsPage),
  ],
})
export class ProductmanualsPageModule {}
