import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RestProvider } from '../../providers/rest/rest';
import { FormBuilder } from '@angular/forms';
import { LoginPage } from '../login/login';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { FileOpener } from '@ionic-native/file-opener';
import { Network } from '@ionic-native/network';
/**
 * Generated class for the ProductmanualsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productmanuals',
  templateUrl: 'productmanuals.html',
})
export class ProductmanualsPage {
  errorMessage: any;
  response: any;
  item: any;
  constructor(private fileOpener: FileOpener,private network: Network,private localNotifications: LocalNotifications, private transfer: FileTransfer, private file: File, private storageService: StorageProvider, public modalCtrl: ModalController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private formBuilder: FormBuilder, private toastCtrl: ToastController) {
    this.productmanual();
    this.localNotifications.on('click').subscribe(notification => {
      console.log("notification click",notification);
      let downloadFileName = this.item.substr(this.item.lastIndexOf('/') + 1); 
      this.fileOpener.open(this.file.externalApplicationStorageDirectory + '/jyoti/' + downloadFileName, 'application/pdf')
         });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductmanualsPage');
  }
  pdfdetail(items) {
    this.showToast(items);
this.item = items.image;
let downloadFileName = this.item.substr(this.item.lastIndexOf('/') + 1); 
    const url = encodeURI(items.image);

    const fileTransfer: FileTransferObject = this.transfer.create();
  
    if(fileTransfer.download == null  ){
     
    }
    else{
      this.fileOpener.open(this.file.externalApplicationStorageDirectory + '/jyoti/' + downloadFileName, 'application/pdf')
      .then(() => console.log('File is opened'))
      .catch(e => this.filedownload());
    }
    
  }
  filedownload(){
    const url = encodeURI(this.item);

    const fileTransfer: FileTransferObject = this.transfer.create();
    
    let downloadFileName = this.item.substr(this.item.lastIndexOf('/') + 1); 
  fileTransfer.onProgress((progressEvent) => {
     
      if (progressEvent.lengthComputable) {
        var perc = Math.floor( progressEvent.total * 100);
        
         
          this.localNotifications.schedule({
            title: 'Download',
            text: 'Download in progress',
            progressBar: { value: perc },
            smallIcon:'res://drawable-land-hdpi/ic_stat.png'
          });
        //drawable-ldpi-icon.png
      }
    });
    fileTransfer.download(url, this.file.externalApplicationStorageDirectory + '/jyoti/' + downloadFileName).then((entry) => {
      console.log('download complete: ' + entry.toURL());
    
      this.localNotifications.schedule({
        title: 'Download',
        text: 'Download complete',
        smallIcon:'res://drawable-land-hdpi/ic_stat.png'
      });



    }, (error) => {
      console.log('error ' + error);
      let alert = this.alertCtrl.create({
        message: 'Oh no! <br> No Internet Found.Check Your Connection ',
        title: '<img src="assets/img/iconmonstr-warning-8.png" class="headerlogo">Product Module',
        buttons: ['ok'],
        cssClass: 'myalert'
      });
      alert.present();
    });
  }
  showToast(items) {
    console.log(items)
    const toast = this.toastCtrl.create({
      message: 'Downloading...',

      duration: 1000
    });

    toast.onDidDismiss(this.dismissHandler);
    toast.present();
  }
  private dismissHandler() {
    console.info('Toast onDidDismiss()');
  }
  productmanual() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.rest.productManual().subscribe(
      res => {
        if (res) {
          this.response = res;
          console.log(this.response);
        }
        loading.dismiss();
      });
  }
  back() {
    this.navCtrl.pop();
  }
}
