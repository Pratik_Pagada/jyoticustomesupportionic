import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BranchofficesPage } from './branchoffices';

@NgModule({
  declarations: [
    BranchofficesPage,
  ],
  imports: [
    IonicPageModule.forChild(BranchofficesPage),
  ],
})
export class BranchofficesPageModule {}
