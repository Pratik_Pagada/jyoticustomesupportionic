import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the BranchofficesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-branchoffices',
  templateUrl: 'branchoffices.html',
})
export class BranchofficesPage {
  branchtab:any;
  reportres:any;
  errorMessage:any;
  loading : any;
 domesticList:any;
 selcttedbranch:any;
 domesticObj:any;
 internationalObj:any;
 inernationalList:any;
 selectintrbrnch:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public rest : RestProvider,public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
    this.branchtab = 'domestic';
	  
	  this.domesticObj={};
	  this.internationalObj= {};
	   this.getdomesticbranch();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BranchofficesPage');
  }
  changetab(){
	  
    if(!this.internationalObj.name){
      this.getinternationalbranch();
    }
    
    }
    
    
    changedomesticbranch(){
      for(let d =0; d<this.domesticList.length; d++){
        if(this.domesticList[d].id==this.selcttedbranch){
        
        this.domesticObj.name = this.domesticList[d].name;  
        this.domesticObj.companyname = this.domesticList[d].companyname;  
        this.domesticObj.maplocation = this.domesticList[d].maplocation;  
        this.domesticObj.email = this.domesticList[d].email;  
        this.domesticObj.phone = this.domesticList[d].phone;  
        
        }
      }
    }
    
    changeinterbranch(){
      for(let i =0; i<this.inernationalList.length; i++){
        if(this.inernationalList[i].id==this.selectintrbrnch){
        
        this.internationalObj.name = this.inernationalList[i].name;  
        this.internationalObj.companyname = this.inernationalList[i].companyname;  
        this.internationalObj.maplocation = this.inernationalList[i].maplocation;  
        this.internationalObj.email = this.inernationalList[i].email;  
        this.internationalObj.phone = this.inernationalList[i].phone;  
        
        }
      }
    }
    
    
    
    
    
    getinternationalbranch() {
      this.showLoading();
   
      this.rest.Internation().subscribe(
       inernationalList => {this.inernationalList = inernationalList;
        this.hideLoading();
      this.selectintrbrnch = this.inernationalList[0].id;
      this.changeinterbranch();
       },
     error =>  {this.errorMessage = <any>error;
        this.hideLoading();
     });
    }
    
    getdomesticbranch() {
      this.showLoading();
   
      this.rest.DomesticList().subscribe(
       domesticList => {this.domesticList = domesticList;
        this.hideLoading();
      this.selcttedbranch = this.domesticList[0].id;
      this.changedomesticbranch();
       },
     error =>  {this.errorMessage = <any>error;
        this.hideLoading();
     });
    }
    
     showLoading(){
      this.loading = this.loadingCtrl.create({
          content: 'Please wait...'
         });
        this.loading.present();
    }
    
    hideLoading(){
      this.loading.dismiss();
    }
    
    showToast(msg) {
      let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
      });
        toast.present();
    }
    
    back(){
     this.navCtrl.pop(); 
    }
}
