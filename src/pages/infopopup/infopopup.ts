import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

/**
 * Generated class for the InfopopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-infopopup',
    templateUrl: 'infopopup.html',
})
export class InfopopupPage {
    errorMessage: any;
    arrFuel = [{
        "id": "94",
        "status": "Petrol"
    },
    {
        "id": "95",
        "status": "Diesel"
    }];
    arrVehicleType = [{
        "id": "90",
        "status": "2 Wheeler"
    },
    {
        "id": "91",
        "status": "4 Wheeler"
    },
    {
        "id": "92",
        "status": "Public Transport"
    }]
    vehicleType = "90"
    allowance = "";
    amount = "";
    FuelType = ""
    lat="";
    long="";
    dvrId: any;
    constructor(private diagnostic: Diagnostic,private geolocation: Geolocation,private storageService: StorageProvider, public rest: RestProvider, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public toastCtrl: ToastController) {
        this.lat = this.navParams.get('lat');
        this.long = this.navParams.get('long');
        this.dvrId = this.navParams.get('dvrId');
        this.arrVehicleType = this.navParams.get('arrVehicle');
        if(this.arrVehicleType.length>0){
            this.vehicleType = this.arrVehicleType[0].id;
        }
        this.arrFuel = this.navParams.get('arrFuel');
        if(this.arrFuel.length>0){
            this.FuelType = this.arrFuel[0].id;
        }

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad InfopopupPage');
        // this.getVehicleType()
    }


    dismissView() {
        this.viewCtrl.dismiss(false);
    }
    changeinterbranch() {
        console.log(this.vehicleType);

    }
    onClickSave() {

        let option={
            enableHighAccuracy:true
        }
        let loading = this.loadingCtrl.create({
            cssClass: 'transparent'
        });
        loading.present();
        this.diagnostic.isGpsLocationEnabled().then((isAvailable) => {
            console.log(isAvailable);
            if (isAvailable) {
        this.geolocation.getCurrentPosition(option).then((resp) => {
            console.log(resp.coords.latitude);
            console.log(resp.coords.longitude);
            this.lat = resp.coords.latitude+"";
            this.long = resp.coords.longitude+"";
            var objData = {
                "iDvrId": this.dvrId,
                "vEndLat": this.lat,
                "vEndLong": this.long,
                "iTransportType": this.vehicleType,
                "iFuelType": "",
                "vAmount": "",
                "vFoodAllowance": ""
            }
            if (this.vehicleType == "92") {

                if (this.amount == "") {
                    this.showToast("Amount required.")
                    return;
                }
                objData.vAmount = this.amount;
            }
            if (this.vehicleType == "91") {
                if (this.FuelType == "") {
                    this.showToast("Fuel type required.")
                    return;
                }
                objData.iFuelType = this.FuelType
            }
            // if (this.allowance == "") {
            //     this.showToast("Other allowance required.")
            //     return;
            // }
            objData.vFoodAllowance = this.allowance;


            this.rest.endDvr(objData).subscribe(
                res => {
                    loading.dismiss();
                    if (res.status == 200) {
                        this.showToast(res.message.success);
                        this.viewCtrl.dismiss(true);
                    }
                },
                error => {
                this.errorMessage = <any>error;
                    loading.dismiss();
                    if (this.errorMessage.status == 401) {
                        this.showToast(this.errorMessage.message.error);
                        this.storageService.setObject("tokan", '');
                        this.navCtrl.setRoot(LoginPage);
                    }
                    else {
                        this.showToast(this.errorMessage.message.error);
                    }

                });
        }).catch((error) => {
            loading.dismiss();
            console.log('Error getting location', error);
            this.showToast("Please enable location service");
        });
    }
    else {
        loading.dismiss();
        this.showToast("Please enable location service");
        this.diagnostic.switchToLocationSettings();
    }
})




    }
    showToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 1000,
            position: 'bottom'
        });
        toast.present();
    }

}
