import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfopopupPage } from './infopopup';

@NgModule({
  declarations: [
    InfopopupPage,
  ],
  imports: [
    IonicPageModule.forChild(InfopopupPage),
  ],
})
export class InfopopupPageModule {}
