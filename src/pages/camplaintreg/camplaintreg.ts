import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController, ModalController, Content } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RestProvider } from '../../providers/rest/rest';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ModalComponent } from '../../components/modal/modal';
import { LoginPage } from '../login/login';
import { MachinemodalComponent } from '../../components/machinemodal/machinemodal';
import { StatemodalComponent } from '../../components/statemodal/statemodal';
import { ComplaintsPage } from '../complaints/complaints';
import { Network } from '@ionic-native/network';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Subscription } from 'rxjs/Subscription';
declare var navigator: any;
declare var Connection: any;
/**
 * Generated class for the CamplaintregPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-camplaintreg',
  templateUrl: 'camplaintreg.html',
})
export class CamplaintregPage {
  @ViewChild(Content) content: Content;

  private Myform: FormGroup;
  loading: any;
  currentDate;
  errorMessage: any;
  allDropList: any;
  customerList: any;
  comapnyslct: any;
  comapnyslctid: any;
  customerObj: any;
  dropdowngen: any;
  dropdownslct: any;
  dropdownslctid: any;
  data: any;
  machineslct: any;
  machineslctid: any;
  data1: any;
  data2: string;
  machinemodelslctid: any;
  machinemodelslct: any;
  data3: string;
  data4: string;
  items: any;
  connected: Subscription;
  disconnected: Subscription;
  companymodallist: any;
  id: any;
  company: any;
  address: any;
  cmpid: any;
  customerid: any;
  productname: any;
  machineserialnumber: any;
  dispatchdate: any;
  uwowamc: any;
  machinestatusid: any;
  item: any;
  mselect: any;
  filter: any;
  compsubject: any;
  loginid: any;
  userid: any;
  companyid: any;
  drpid: any;
  complainid: any;
  constructor(private network: Network, private sqlite: SQLite, private storageService: StorageProvider, public modalCtrl: ModalController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private formBuilder: FormBuilder, private toastCtrl: ToastController) {
    this.Myform = this.formBuilder.group({
      comapnyname: ['', Validators.required],
      address: ['', Validators.required],
      dropdownlist: ['', Validators.required],
      dtl: ['', Validators.required],
      machinemodelslct: ['', Validators.required],
      contact_person: [''],
      tel: [''],
      data2: [''],
      data3: [''],
      data4: ['']
    });

    let currentdate = new Date();
    let crntMonth = currentdate.getMonth();
    crntMonth = crntMonth + 1;

    this.currentDate = currentdate.getDate() + "/" + crntMonth + "/" + currentdate.getFullYear();
    this.data2 = "";
    this.data3 = "";
    this.data4 = "";
    this.data1 = {};
    this.data = {};
    this.customerObj = {};
    this.getcompanyList();
    this.dropdownlistgen();

    this.storageService.getObject('id').then((val) => {
      if (val) {
        this.loginid = val;


        this.sqlite.create({
          name: 'salesdb.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
          db.executeSql("SELECT * FROM companylistoffline WHERE userid=? LIMIT 1", [this.loginid]).then((data) => {
            this.items = [];
            if (data.rows.length > 0) {
              for (var i = 0; i < data.rows.length; i++) {
                this.items.push(data.rows.item(i));
                //  console.log(this.items[0].response);
                this.userid = JSON.parse(this.items[i].userid);
                this.item = JSON.parse(this.items[i].response);
                //console.log(this.item);
                this.customerList = this.item;

              }
            }
          }, (e) => {

            console.log("Errot: " + JSON.stringify(e));
          });
        });
      }
    });
  }
  selectGotram() {
    this.showLoading();
    let data = { "title": "Company", "data": this.customerList }
    this.hideLoading();
    let modal = this.modalCtrl.create(ModalComponent, data);
    this.hideLoading();
    modal.onDidDismiss(data => {

      if (data) {
        this.comapnyslct = data.company;
        this.comapnyslctid = data.id;
        this.getcustomerdtl(this.comapnyslctid);
      }
    });
    this.hideLoading();
    modal.present();
  }

  selectlist() {

    let data = { "title": "Complain Subject", "data": this.dropdowngen }
    let modal = this.modalCtrl.create(StatemodalComponent, data);
    modal.onDidDismiss(data => {

      if (data) {
        this.dropdownslct = data.status;
        this.dropdownslctid = data.id;

      }
    });
    modal.present();
  }
  selectmachine() {

    let data = { "title": "Machine Model Select", "data": this.machineslct }
    let modal = this.modalCtrl.create(MachinemodalComponent, data);
    modal.onDidDismiss(data => {

      if (data) {
        this.machinemodelslct = data.product_name;
        this.machinemodelslctid = data.id;
        this.data2 = data.uw_ow_amc;
        this.data3 = data.machine_serial_number;
        this.data4 = data.dispatch_date;

      }
    });
    modal.present();
  }
  dropdownlistgen() {

    var networkState = navigator.connection.type;

    if (networkState === Connection.NONE) {
      console.log("offline", networkState);


      this.sqlite.create({
        name: 'salesdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql("SELECT * FROM complainsubject", []).then((data) => {
          this.items = [];
          if (data.rows.length > 0) {
            for (var i = 0; i < data.rows.length; i++) {
              this.items.push(data.rows.item(i));
              this.compsubject = JSON.parse(this.items[0].respstatus)
              // console.log(this.compsubject);
              this.dropdowngen = this.compsubject;

            }
          }
        }, (e) => {

          console.log("Errot: " + JSON.stringify(e));
        });
      });


    } else {
      console.log("online", networkState);
      this.rest.alldropdownlistgen().subscribe(data => {
        if (data.status == 200) {
          this.dropdowngen = data.data;
        }
      }, error => {
        this.errorMessage = <any>error;
        this.showToast(this.errorMessage.message.error);
      }
      )
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CamplaintregPage');

  }
  ionViewDidEnter() {
    this.connected = this.network.onConnect().subscribe(data => {
      console.log(data)
      this.findAll();
    }, error => console.error(error));

    this.disconnected = this.network.onDisconnect().subscribe(data => {
      console.log(data)

    }, error => console.error(error));
  }
  ionViewWillLeave() {
    this.connected.unsubscribe();
    this.disconnected.unsubscribe();
  }

  getcompanyList() {
    this.showLoading();
    this.rest.companiesbyengineer().subscribe(data => {
      this.hideLoading();
      this.allDropList = data;
      if (this.allDropList.status == 200) {
        this.customerList = this.allDropList.data;
      }
    },
      error => {
        this.errorMessage = <any>error;
        this.hideLoading();
        this.showToast(this.errorMessage.message.error);
      });
  }
  companyChange(val) {
    if (val) {
      this.dropdownslct = null;
      this.dropdownslctid = null;
      this.machinemodelslct = null;
      this.machinemodelslctid = null;
      this.data2 = null;
      this.data3 = null;
      this.data4 = null;
    }
  }
  getcustomerdtl(cid) {
    var networkState = navigator.connection.type;

    if (networkState === Connection.NONE) {
      console.log("offline", networkState);

      // this.item.companymodels = this.mselect;
      // this.machineslct = this.mselect
      // console.log(cid);
      this.filter = this.item.filter(item => item.id === cid);
      // console.info(this.filter[0].companymodels);
      this.customerObj = this.filter[0];
      this.machineslct = this.filter[0].companymodels;
      //console.info(this.machineslct);

    } else {
      console.log("online", networkState);
      this.rest.customerDetail(cid).subscribe(data => {


        if (data.status == 200) {
          this.customerObj = data.data[0];
          console.log(this.customerObj);

          this.data = data.data[1].modellist;


          var d = 0;
          for (let i = 0; i < this.data.length; i++) {
            if (this.data[0]) {
              d++;
              this.machineslct = this.data[i];
              this.machineslct[i].optionvalues;

            }

          }

        }
      },
        error => {
          this.errorMessage = <any>error;
          this.hideLoading();
          if (this.errorMessage.status == 401) {
            this.storageService.setObject("tokan", '');
            this.navCtrl.setRoot(LoginPage);
          }
          this.showToast(this.errorMessage.message.error);

        });
    }


  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  MyformSubmit() {
    if (this.Myform.valid) {
      this.showLoading();
      var networkState = navigator.connection.type;

      if (networkState === Connection.NONE) {
        this.hideLoading();
        console.log("offline", networkState);
        
        this.sqlite.create({
          name: 'salesdb.db',
          location: 'default'
        }).then((db: SQLiteObject) => {
         // console.log("this login is id confirm",this.loginid);
          db.executeSql('INSERT INTO compainoffline VALUES(?,?,?,?,?,?,?,?,?)', [, this.comapnyslctid, this.Myform.value.address, this.dropdownslctid, this.Myform.value.dtl, this.machinemodelslctid, this.Myform.value.contact_person, this.Myform.value.tel,this.loginid])
            .then(res => {
              console.log(res);

            })
            .catch(e => {
              console.log(e);

            });
        }).catch(e => {
          console.log(e);

        }); 
     

      } else {
        console.log("online", networkState);

        this.rest.Selfcomplain(this.Myform.value, this.comapnyslctid, this.dropdownslctid, this.machinemodelslctid).subscribe(data => {
          this.Myform.reset();
          this.hideLoading();
          if (data.status == 200) {
            this.navCtrl.push(ComplaintsPage);
            this.showToast(data.message.success);


          }
        },
          error => {
            this.errorMessage = <any>error;
            this.hideLoading();
            if (this.errorMessage.status == 401) {

              this.storageService.setObject("tokan", '');
              this.navCtrl.setRoot(LoginPage);
            }
            this.showToast(this.errorMessage.message.error);
          });

      }


    } else {
      this.validateAllFormFields(this.Myform); //{7}
      this.content.scrollToTop();
    }
  }
  public findAll() {
    this.sqlite.create({
      name: 'salesdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      console.log("loginid check",this.loginid);
      db.executeSql("SELECT * FROM compainoffline WHERE userid=?",[this.loginid]).then((data) => {
        this.items = [];
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            this.items.push(data.rows.item(i));
            //console.log(this.items);
            this.rest.Selfcomplainoffline(this.items[i]).subscribe(data => {
              this.Myform.reset();
              this.hideLoading();
              if (data.status == 200) {

                this.sqlite.create({
                  name: 'salesdb.db',
                  location: 'default'
                }).then((db: SQLiteObject) => {
                  for (var i = 0; i < this.items.length; i++) {
                    //console.log(this.items[i].complain_id);
                    db.executeSql('DELETE FROM compainoffline WHERE complain_id=?', [this.items[i].complain_id])
                      .then(res => {
                        console.log(res);

                      })
                      .catch(e => console.log(e));
                  }
                }).catch(e => console.log(e));
                this.navCtrl.push(ComplaintsPage);
                this.showToast(data.message.success);

                //DELETE from complain where complainid
              }
            },
              error => {
                this.errorMessage = <any>error;
                this.hideLoading();
                if (this.errorMessage.status == 401) {

                  this.storageService.setObject("tokan", '');
                  this.navCtrl.setRoot(LoginPage);
                }
                this.showToast(this.errorMessage.message.error);
              });
          }
        }
      }, (e) => {

        console.log("Errot: " + JSON.stringify(e));
      });
    });
  }



  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }
  back() {
    this.navCtrl.pop();
  }
}
