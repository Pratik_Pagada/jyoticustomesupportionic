import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CamplaintregPage } from './camplaintreg';

@NgModule({
  declarations: [
    CamplaintregPage,
  ],
  imports: [
    IonicPageModule.forChild(CamplaintregPage),
  ],
})
export class CamplaintregPageModule {}
