import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Content, ToastController, ModalController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { StorageProvider } from '../../providers/storage/storage';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';


/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  @ViewChild(Content) content: Content;


  private profileform: FormGroup;
  editprofileform: any;
  errorMessage: any;
  loading: any;
  constructor(private storageService: StorageProvider,private alertCtrl: AlertController,  public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private formBuilder: FormBuilder, private toastCtrl: ToastController) {

    this.editprofileform = {};
    this.profileform = this.formBuilder.group({
      firstname: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*')])],
      lastname: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*')])],
      phone: ['', Validators.required]
    });

    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }




  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  profileformSubmit() {
    if (this.profileform.valid) {
      this.showLoading();
      this.rest.profile(this.profileform.value).subscribe(data => {
        this.hideLoading();
        if (data.status == 200) {

          this.presentAlert(data.message.success);
        }
      },
        error => {
          this.errorMessage = <any>error;
          this.hideLoading();
          if (this.errorMessage.status == 401) {
            this.storageService.setObject("tokan", '');
            this.navCtrl.setRoot(LoginPage);
          }
          this.presentAlert(this.errorMessage.message.error);
        });

    } else {
      this.validateAllFormFields(this.profileform); //{7}
      this.content.scrollToTop();
    }
  }
  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      message: msg,
      title:'<img src="assets/img/iconmonstr-warning-8.png" class="headerlogo">Edit Profile!!',
      buttons: ['ok'],
      cssClass: 'myalert'
    });
    alert.present();
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  back() {
    this.navCtrl.pop();
  }

}


