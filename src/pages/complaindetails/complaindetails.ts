import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RestProvider } from '../../providers/rest/rest';
import { LoginPage } from '../login/login';
import { ComplainpicxPage } from '../complainpicx/complainpicx';

/**
 * Generated class for the ComplaindetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-complaindetails',
  templateUrl: 'complaindetails.html',
})
export class ComplaindetailsPage {
  cmpid: any;
  comlist: any;
  errorMessage: any;
  complist: any;

  constructor(private storageService: StorageProvider, public modalCtrl: ModalController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
 
  this.comlist =this.navParams.get('lead');
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ComplaindetailsPage',);
    console.log(this.comlist);
  }
  clickform(cid){
    this.navCtrl.push(ComplainpicxPage,{'comlist':cid});
  }

  back() {
    this.navCtrl.pop();
  }

}
