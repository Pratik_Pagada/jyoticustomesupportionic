import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController, ToastController, ModalController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RestProvider } from '../../providers/rest/rest';
import { LoginPage } from '../login/login';
import { ModalComponent } from '../../components/modal/modal';

/**
 * Generated class for the UpdateLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-update-location',
  templateUrl: 'update-location.html',
})
export class UpdateLocationPage {
  arrVisitType=[];
  arrCustomer=[];
  selectedVisit:any;
  selectedCustomerId:any;
  Purpose="";
  visit:any;
  selectedCustomerText="";
  errorMessage:any;
  constructor(public modalCtrl:ModalController,private storageService: StorageProvider, public rest: RestProvider, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public toastCtrl: ToastController) {
    this.arrVisitType = navParams.get('arrVisitType');
    this.arrCustomer = navParams.get('arrCustomer');
    this.visit=navParams.get('visit');
    this.selectedVisit = navParams.get('selectedVisit');
    this.Purpose = navParams.get('Purpose');
    this.selectedCustomerId = navParams.get('selectedCustomerId');
    if(this.arrVisitType.length>0){
     if(this.selectedVisit == ""){
       this.selectedVisit = this.arrVisitType[0].id
     }
    }
    if(this.arrCustomer.length>0){
      if(this.selectedCustomerId == ""){
        this.selectedCustomerId = this.arrCustomer[0].id
      }
      else{
          this.selectedCustomerText = this.visit.company;
      }
     }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateLocationPage');
  }

  dismissView(){
    this.viewCtrl.dismiss(false);
  }
  onClickSave() {
    var objData = {
        "iLocationId": this.visit.iLocationId,
        "iVisitType": this.selectedVisit,
        "iCustomerId": "",
        "vPurpose": "",
    }
    if (this.selectedVisit == "136") {

        if (this.selectedCustomerId == "") {
            this.showToast("Customer Names is required.")
            return;
        }
        objData.iCustomerId = this.selectedCustomerId;
    }
    if (this.selectedVisit == "138") {
        if(this.Purpose == ""){
        this.showToast("Purpose is required.")
        return;
        }
    }
    objData.vPurpose = this.Purpose;

    let loading = this.loadingCtrl.create({
        cssClass: 'transparent'
    });
    loading.present();
    this.rest.updateLocation(objData).subscribe(
        res => {
            loading.dismiss();
            if (res.status == 200) {
                this.showToast(res.message.success);
                this.viewCtrl.dismiss(true);
            }
        },
        error => {
        this.errorMessage = <any>error;
            loading.dismiss();
            if (this.errorMessage.status == 401) {
                this.showToast(this.errorMessage.message.error);
                this.storageService.setObject("tokan", '');
                this.navCtrl.setRoot(LoginPage);
            }
            else {
                this.showToast(this.errorMessage.message.error);
            }

        });
}
selectGotram() {
    let data = { "title": "Compalint No. - Customer Name", "data": this.arrCustomer,type:"complain" }
    let modal = this.modalCtrl.create(ModalComponent, data);
    modal.onDidDismiss(data => {
      if (data) {
        this.selectedCustomerText = data.company;
        this.selectedCustomerId = data.id;

      }
    });
    modal.present();
  }
  showToast(msg) {
    let toast = this.toastCtrl.create({
        message: msg,
        duration: 1000,
        position: 'bottom'
    });
    toast.present();
}
}
