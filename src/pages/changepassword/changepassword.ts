import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Content, AlertController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RestProvider } from '../../providers/rest/rest';
import { LoginPage } from '../login/login';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

/**
 * Generated class for the ChangepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {
  @ViewChild(Content) content: Content;
  private pwdfrom: FormGroup;
  errorMessage: any;
  user: any;
  loading: any;
  constructor(private storageService: StorageProvider,private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider, public loadingCtrl: LoadingController, private formBuilder: FormBuilder, private toastCtrl: ToastController) {
    this.user = {};

    this.pwdfrom = formBuilder.group({
      password: ['', Validators.required],
      newpassword: ['', Validators.required],
      confirmpassword:['', Validators.compose([Validators.required,Validators.pattern('^(?=.*?[0-9]).{6}$')])]
    });
  }
  
  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
      let passwordInput = formGroup.get['newpassword'],
      passwordConfirmationInput = formGroup.controls['confirmpassword'];
  if (passwordInput.value !== passwordConfirmationInput.value) {
    return passwordConfirmationInput.setErrors({notEquivalent: true})
  }
  else {
      return passwordConfirmationInput.setErrors(null);
  }           
      }
    this.validateAllFormFields
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepasswordPage');
  }
  pwdbtn() {
    if (this.pwdfrom.valid) {
      this.showLoading();
      this.rest.changepwd(this.user).subscribe(data => {
        this.hideLoading();
        if (data.status == 200) {

          this.presentAlert(data.message.success);
        }
      },
        error => {
          this.errorMessage = <any>error;
          this.hideLoading();
          if (this.errorMessage.status == 401) {
            this.storageService.setObject("tokan", '');
            this.navCtrl.setRoot(LoginPage);
          }
          this.presentAlert(this.errorMessage.message.error);
        });

    } else {
      this.validateAllFormFields(this.pwdfrom); //{7}
      this.content.scrollToTop();
    }
  }
  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      message: msg,
      title:'<img src="assets/img/iconmonstr-warning-8.png" class="headerlogo">Change Password!!',
      buttons: ['ok'],
      cssClass: 'myalert'
    });
    alert.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  back() {
    this.navCtrl.pop();
  }
}
