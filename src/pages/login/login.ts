import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, AlertController, MenuController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { StorageProvider } from '../../providers/storage/storage';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  resposeData: any;
  loginreg = { username: '', password: '' };
  loading: any;
  errorMessage: any;
  constructor(public navCtrl: NavController,private menu: MenuController,private alertCtrl: AlertController, public navParams: NavParams, public loadingCtrl: LoadingController, public rest: RestProvider, private toastCtrl: ToastController, private storageService: StorageProvider) {

  }
  async ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.menu.swipeEnable(false);
    await this.storageService.getObject('tokan').then((val) => {
      if (val) {
        this.navCtrl.setRoot(HomePage);
      }
    });
  }
  async loginbtn() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    await loading.present();
    this.rest.login(this.loginreg).subscribe(
      data => {
        this.resposeData = data;

        if (this.resposeData.status == 200) {
            this.storageService.setObject('id', this.resposeData.data.id);
            loading.dismiss();
          this.storageService.setObject('tokan', this.resposeData.data.vAuthToken).then(() => {
            this.navCtrl.setRoot(HomePage);
          })



        }
      },
      error => {
        this.errorMessage = <any>error;
        loading.dismiss();
        this.presentAlert(this.errorMessage.message.error);

      });
  }
presentAlert(msg) {
  let alert = this.alertCtrl.create({
    message: msg,
    title:'<img src="assets/img/iconmonstr-warning-8.png" class="headerlogo"> Login!!',
    buttons: ['ok'],
    cssClass: 'myalert'
  });
  alert.present();
}


  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
}



